from st3m.application import Application, ApplicationContext
from st3m.ui.view import View
from st3m.input import InputController
#import bl00mbox
#from st3m.reactor import Responder
import st3m.run
import leds
import os
import sys
import random

class Windows(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.windows_image_path = ""
        self.bsod = False
        self.windows_image_path = "/flash/sys/apps/windows-nick/windows-nick.png"
        self.info_image_path = "/flash/sys/apps/windows-nick/windows-nick-info.png"
        self.timer = 0
        self.info = False

        # optional static LED pattern to save battery
        for led in range(0,3):
            leds.set_rgb(led, 1.0, 0.0, 0.0)
        for led in range(3,12):
            leds.set_rgb(led, 0.0, 1.0, 0.0)
        for led in range(12,21):
            leds.set_rgb(led, 1.0, 1.0, 0.0)
        for led in range(21,30):
            leds.set_rgb(led, 0.0, 0.0, 1.0)
        for led in range(30,40):
            leds.set_rgb(led, 1.0, 0.0, 0.0)
        leds.update()

    def draw(self, ctx) -> None:
        # Paint the background black
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        if self.info == True:
            ctx.image(self.info_image_path, -120, -120, 240, 240)
        else:
            ctx.image(self.windows_image_path, -120, -120, 240, 240)

    def think(self, ins, delta_ms: int) -> None:
        super().think(ins, delta_ms) # Let Application do its thing
        # LED pattern that continuously updates
        '''
        self.timer += delta_ms
        if self.timer > 800:
            leds.set_rgb(random.randint(0,39),random.random(),random.random(),random.random())
            leds.update()
            self.timer = 0
        '''
        if ins.buttons.app in [-1, 1, 2]:
            self.info = True
        else:
            self.info = False

if __name__ == '__main__':
    st3m.run.run_view(Windows(ApplicationContext()))

